.TH IOX-ROUDI 1 "" "@VERSION@" "Eclipse Iceoryx"
.SH NAME
iox-roudi \- Iceoryx shared memory management daemon
.SH SYNOPSIS
.SY iox-roudi
.OP \-u ID
.OP \-m MODE
.OP \-l LEVEL
.OP \-x COMPAT
.OP \-k DELAY
.OP \-c FILE
.YS
.SH DESCRIPTION
.B iox-roudi
is the shared memory management daemon that enables processes to participate
in the local Iceoryx data exchange.
.SH OPTIONS
.TP
.BR \-u " " \fIID\fR ", " \-\-unique\-roudi\-id " " \fIID\fR
set the unique RouDi id.
.TP
.BR \-m " " \fIMODE\fR ", " \-\-monitoring\-mode " " \fIMODE\fR
set process alive monitoring mode to
.BR on " (default)"
or
.BR off .
.TP
.BR \-l " " \fILEVEL\fR ", " \-\-log\-level " " \fILEVEL\fR
set the log level to one of
.BR off ", " fatal ", " error ", " warning ", " info ", " debug ", or " verbose .
.TP
.BR \-x " " \fICOMPAT\fR ", " \-\-compatibility " " \fICOMPAT\fR
check the compatibility between runtime libraries and RouDi. Valid values are
.BR off ", " major ", " minor ", " patch ", " commitId ", or " buildDate .
.TP
.BR \-k " " \fIDELAY\fR ", " \-\-kill\-delay " " \fIDELAY\fR
set the delay in seconds until RouDi sends
.B SIGKILL
if a process did not respond to
.BR SIGTERM .
.TP
.BR \-c " " \fIFILE\fR ", " \-\-config\-file " " \fIFILE\fR
Load the configuration file
.IR FILE .
By default, RouDi will look for a valid configuration in
.I /etc/iceoryx/roudi_config.toml
and fall back to hard-coded settings otherwise.
.SH SEE ALSO
.BR iox-introspection-client (1)
.SH NOTE
This manual page was written for Debian by Timo R\[u00F6]hling and may
be used without restriction.
