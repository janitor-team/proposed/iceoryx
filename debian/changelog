iceoryx (2.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.0.2+dfsg

 -- Timo Röhling <roehling@debian.org>  Tue, 19 Apr 2022 11:07:51 +0200

iceoryx (2.0.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.0.1+dfsg

 -- Timo Röhling <roehling@debian.org>  Mon, 04 Apr 2022 12:35:36 +0200

iceoryx (2.0.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * Reintroduce libatomic interface linking for iceoryx_hoofs

 -- Timo Röhling <roehling@debian.org>  Thu, 17 Mar 2022 11:09:09 +0100

iceoryx (2.0.0+dfsg-1) experimental; urgency=medium

  * New upstream version 2.0.0+dfsg
  * Update patches.
    Drop patches which are obsolete or have been applied upstream:
    - Use-Multi-Arch-compliant-install-destinations
    - Fix-format-string-issues
    - Fix-typos
    - Add-option-to-link-against-libatomic
    - Add-CXX_STANDARD-fallback
  * ABI changes
    - libiceoryx_utils has been renamed to libiceoryx_hoofs
    - SONAME bumped to 2

 -- Timo Röhling <roehling@debian.org>  Wed, 16 Mar 2022 17:34:58 +0100

iceoryx (1.0.1+dfsg-6) unstable; urgency=medium

  * Make libatomic a public dependency on selected architectures

 -- Timo Röhling <roehling@debian.org>  Fri, 11 Feb 2022 14:18:17 +0100

iceoryx (1.0.1+dfsg-5) unstable; urgency=medium

  * Sourceful upload to fix inconsistent SOURCE_DATE_EPOCH from
    BinNMU, which breaks M-A: same for libiceoryx-posh-dev.

 -- Timo Röhling <roehling@debian.org>  Mon, 07 Feb 2022 17:39:08 +0100

iceoryx (1.0.1+dfsg-4) unstable; urgency=medium

  * Add missing -latomic links
  * Add missing CXX_STANDARD fallback for non-Linux systems

 -- Timo Röhling <roehling@debian.org>  Mon, 24 Jan 2022 19:21:57 +0100

iceoryx (1.0.1+dfsg-3) unstable; urgency=medium

  * Link against libatomic on powerpc and riscv64

 -- Timo Röhling <roehling@debian.org>  Mon, 24 Jan 2022 15:59:25 +0100

iceoryx (1.0.1+dfsg-2) unstable; urgency=medium

  * Source-only upload.
  * Ignore test results for now.
    There are some issues with shared memory on buildd.

 -- Timo Röhling <roehling@debian.org>  Sun, 23 Jan 2022 22:38:11 +0100

iceoryx (1.0.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #998146)

 -- Timo Röhling <roehling@debian.org>  Tue, 02 Nov 2021 12:07:58 +0100
